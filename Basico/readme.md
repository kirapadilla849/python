# PYTHON A NIVEL BASICO.
## Requisitos
Debes de tener instalado los siguientes programas:
* [VSC](https://code.visualstudio.com/): Visual Studio Code
* [Python](https://www.python.org/): Python
* [Plugin Live Share](https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare-pack): Plugins de programación colaborativa en remoto
## Videos de ayuda
Debes de tener instalado los siguientes programas:
* [¿Qué es python?](https://youtu.be/ROXckh7y8Iw?list=PL9xn53JF6mUE27ffVJs6olEnmmFVzyc7s)
* [Instalación de VSC y Pyhton](https://youtu.be/SDijL0QXhKM?list=PL9xn53JF6mUE27ffVJs6olEnmmFVzyc7s)
* [Plugin Live Share](https://www.youtube.com/watch?v=nj535VbE9pQ)

