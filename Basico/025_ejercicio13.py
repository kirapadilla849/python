password=input("Introduzca su contraseña: ")
lista=[]
cot1=0
cot2=0
if password=="1234567BO":
    print("Ingresaste al Sistema")
    while(True):
        valor=input("Introduce un número entre 0 a 100: ")
        if valor=="salir":
            break
        else:
            if int(valor)>=0 and int(valor)<=100:
                lista.append(int(valor))
                cot1=cot1+1
            else:
                cot2=cot2+1
else:
    print("No eres usuario del sistema")

suma=0
if cot1!=0:        
    for i in lista:
        suma=suma+i
    promedio=suma/cot1

    print("_____________________________________________________________")
    print("La cantidad de números introducidos entre el rango de 0-100 es :", cot1)
    print("La cantidad de números introducidos fuera del rango de 0-100 es:", cot2)
    print("La suma de los valores introducidos es :", suma)
    print("El promedio de los valores introducidos :", promedio)
else:
    print("Usted no ingreso ningun valor entre el rango de 0-100. Error de división por cero")