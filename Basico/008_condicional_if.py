# Sentencia if
if "12345"=="12345":
    print("Usuario válido")

edad=input("Introduzca su edad:")
if(edad>="18"):
    print("Usted es mayor de edad")
    if(edad<18 & edad>0):
        print("Usted es Menor de edad")
        if(edad<0):
            print("Porfavor coloque un numero valido")

 # otra manera   

if(edad>="18"):
    print("Usted es mayor de edad")
if(int(edad)<18 and int(edad)>=0):
    print("Usted es Menor de edad")
if(int(edad)<0):
    print("Porfavor coloque un numero valido")


# Sentencia if else
if "12345"=="123456":
    print("Usuario válido")
else:
    print("Usuario invalido")

# Sentencia if elif y else
dia="5"
if "1"==dia:
    print("Lunes")
elif "2"==dia:
    print("Martes")
elif "3"==dia:
    print("Miercoles")
elif "4"==dia:    
    print("Jueves")
elif "5"==dia:    
    print("Viernes")
else:  
    print("Día fuera de la semana")

