#importamos la libreria ply.lex como lex
import ply.lex as lex

import ply.yacc as yacc


# lista de tokens
tokens = ['MAS',
         'MENOS',
         'MULTIPLICACION',
         'DIVISION',
         'NUMERO',
         'BINARIO',
         'COMENTARIO',
         #PALABRAS RESERVADAS
         'TIPOENTERO',
         'CHAR',
         'FLOAT',

         #CADENA
         "CADENA"
         ] 



# Reglas simples
t_MAS=r"\+"
t_MENOS=r"\-"
t_MULTIPLICACION=r"\*"
t_DIVISION=r"\/"

t_TIPOENTERO="^(int)$"
t_FLOAT="^(float)$"

# Reglas complejas   
def t_NUMERO(token):
    r"[1-9][0-9]*"
    token.value=int(token.value)
    return token

def t_BINARIO(token):
    r"[0-1]+"
    return token

def t_CADENA(token):
    r"\'[a-zA-Z0-9]*\'" # funcion incompleta
    return token 

# Error handling rule
def t_error(t):
    print("No es parte del lenguaje '%s'" % t.value[0])
    t.lexer.skip(1)

# Crea un objeto lexer para el analisis lexico
lexer = lex.lex()

# Introducimos la cadena a ser evaluada
texto=input("introduzca una cadena: ")
lexer.input(texto)

# verificar en un bucle las cadenas validas con respecto a las reglas
while True:
    tok = lexer.token()
    if not tok: 
        break      # si no hay mas tokens termina el analisis
    print(tok)