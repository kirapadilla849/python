import numpy as np
import matplotlib.pyplot as plt
import cv2
import os
imagen = cv2.imread(os.path.dirname(__file__)+'\\colores.jpg')
# Convertimos la imagen a RGB, porque OpenCV la carga en BGR
imagen = cv2.cvtColor(imagen, cv2.COLOR_BGR2RGB)
# Creamos las máscaras para los colores rojo, verde y azul
mascara_azul = cv2.inRange(imagen, np.array([34, 70, 206]), 
                           np.array([36, 90, 227]))
imagen_roja = cv2.bitwise_and(imagen, imagen, mask=mascara_azul)
# Dibujamos la imagen original y la imagen filtrada
plt.subplot(121)
plt.imshow(imagen)
plt.title('Imagen Original')

plt.subplot(122)
plt.imshow(imagen_roja)
plt.title('Imagen Roja')
plt.show()