from tkinter import Tk, Label, Button


class Motor:
    numero_motor=None
    caballos_fuerza=None
    def __init__(self,n_motor,c_fuerza):
        self.numero_motor=n_motor
        self.caballos_fuerza=c_fuerza
        
    def obtener_n_motor(self,valor1=None,valor2=None):
        return int(valor1) + int(valor2)

    def obtener_c_fuerza(self):        
        return self.caballos_fuerza
    
    def mostrar_datos(self):
        print(self.numero_motor)
        print(self.caballos_fuerza)


class Vehiculo(Motor):
    modelo=None    
    n_ventana=None
    n_placa=None
    color=None
    n_ruedas=None
    n_asientos=None
    estado=False
    def __init__(self,color,n_ventana,n_placa,modelo,n_ruedas,n_asientos,nmotor,cfuerza):        
        Motor.__init__(self,nmotor,cfuerza)
        self.color=color
        self.n_ventana=n_ventana     
        self.n_placa=n_placa
        self.modelo=modelo
        self.n_asientos=n_asientos
        self.n_ruedas=n_ruedas   

    # def __init__(self,color,nmotor,cfuerza):                
    #     Motor.__init__(self,nmotor,cfuerza)
    #     self.color=color
        
    def encender(self):
        self.estado=True
    
    def acelerar(self):
        if self.estado==True:
            print("vehiculo esta acelerando...")
        else:
            print("el vehiculo esta apagado")
    
    def mostrarcolor(self):
        print(self.color)
    
    def mostrar_datos(self):
        print("Modelo ",self.modelo)
        print("Placa ",self.n_placa)
        print("Color ",self.color)
