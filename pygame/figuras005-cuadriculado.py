# Importar la librería de Juegos
import pygame
# Inicia la libreía.
pygame.init()
# Crear una ventana de 1024X764
ventana=pygame.display.set_mode((800,600))
# Reloj de sistema desde pygame
reloj=pygame.time.Clock()
# velocidad de fps
velocidad=50
# ========================================
# Bucle principal
while True:
    # mover 1 fotograma en un segundo
    reloj.tick(velocidad)
    # Bucle para pedir eventos    		
    for evento in pygame.event.get():        
        # Compara si se presionó el boton X de la ventana
        if evento.type==pygame.QUIT:                
            # Salir
            quit()  
    # =======================
    # PINTADO Y DIBUJADO
    # Pintar de color (R,G,B) El fondo
    ventana.fill((0,80,150))
    #========================
    # LOGICA DEL PROGRAMA
    for pos_x in range(0,800,25):
        for pos_y in range(0,600,25):
            pygame.draw.rect(ventana, (255,255,255), (pos_x,pos_y,25,25),1)           
    # Actualiza la ventana, constantemente...
    pygame.display.update()