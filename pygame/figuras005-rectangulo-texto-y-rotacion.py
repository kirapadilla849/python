import pygame
FPS = 30  

# Define colores
NEGRO = (0 , 0 , 0)  
AZUL = (0 , 0 , 200)
VERDE = (0 , 255 , 0)  

# Inicializa los modulos de pygame
pygame.init()  
screen = pygame.display.set_mode((800,600))  
# for setting FPS  
clock = pygame.time.Clock()  

rot = 0  
rot_speed = 2  

# Defina una superficie 
image_orig = pygame.Surface((200 , 100))  
# Para crear un fondo transparente al girar una imagen
image_orig.set_colorkey(NEGRO)  
# Rellene el rectángulo / superficie con color verde
image_orig.fill(VERDE)  
# Crear una copia de la imagen de Orignal para una rotación suave
image = image_orig.copy()


# define rect for placing the rectangle at the desired position  
rect = pygame.Rect(10,10,300,200)
rect.center = (800 // 3 , 600 // 2)  
# keep rotating the rectangle until running is set to False  
running = True  
while running:  
    # set FPS  
    clock.tick(FPS)  
    # clear the screen every time before drawing new objects  
    screen.fill(NEGRO)  
    # check for the exit  
    for event in pygame.event.get():  
        if event.type == pygame.QUIT:  
            running = False  

    # making a copy of the old center of the rectangle  
    old_center = rect.center  
    # defining angle of the rotation  
    rot = (rot + rot_speed) % 360  
    # rotating the orignal image  
    new_image = pygame.transform.rotate(image_orig , rot)  
    rect = new_image.get_rect()  
    # set the rotated rectangle to the old center  
    rect.center = old_center  
    # drawing the rotated rectangle to the screen  
    screen.blit(new_image , rect)  
    # flipping the display after drawing everything  
    pygame.display.flip()  

pygame.quit() 