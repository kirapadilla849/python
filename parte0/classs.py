class Caja:        

    def __init__(self,b200=0,b100=0,b50=0,b20=0,b10=0,m5=0,m2=0,m1=0,m050=0,m020=0,m010=0):            
        self.billete_200=b200
        self.billete_100=b100
        self.billete_50=b50
        self.billete_20=b20
        self.billete_10=b10

        self.moneda_5=m5
        self.moneda_2=m2
        self.moneda_1=m1
        self.moneda_050=m050
        self.moneda_020=m020
        self.moneda_010=m010

    def calcular_caja(self):
        return self.billete_200*200+self.billete_100*100+self.billete_50*50+self.billete_20*20+self.billete_10*10+self.moneda_5*5+self.moneda_2*2+self.moneda_1*1+self.moneda_050*0.5+self.moneda_020*0.2+self.moneda_010*0.1
    
    def mostrar_billetes(self):
        print("Hay ",self.billete_200,' de 200 Bs.-')
        print("Hay ",self.billete_100,' de 100 Bs.-')
        print("Hay ",self.billete_50,' de 50 Bs.-')
        print("Hay ",self.billete_20,' de 20 Bs.-')
        print("Hay ",self.billete_10,' de 10 Bs.-')
    def mostrar_billetes_detalle(self):
        print("Hay ",self.billete_200,' de 200 Bs.-',' = ',self.billete_200*200)
        print("Hay ",self.billete_100,' de 100 Bs.-',' = ',self.billete_100*100)
        print("Hay ",self.billete_50,' de 50 Bs.-',' = ',self.billete_50*50)
        print("Hay ",self.billete_20,' de 20 Bs.-',' = ',self.billete_20*20)
        print("Hay ",self.billete_10,' de 10 Bs.-',' = ',self.billete_10*10)        

    def mostrar_monedas(self):
        print("Hay ",self.moneda_5,' de 5 Bs.-')
        print("Hay ",self.moneda_2,' de 2 Bs.-')
        print("Hay ",self.moneda_1,' de 1 Bs.-')
        print("Hay ",self.moneda_050,' de 50 Cntvs.-')
        print("Hay ",self.moneda_020,' de 20 Cntvs.-')        
        print("Hay ",self.moneda_010,' de 10 Cntvs.-')        
    
    def mostrar_monedas_detalle(self):
        print("Hay ",self.moneda_5,' de 5 Bs.-',' = ',self.moneda_5*5)
        print("Hay ",self.moneda_2,' de 2 Bs.-',' = ',self.moneda_2*2)
        print("Hay ",self.moneda_1,' de 1 Bs.-',' = ',self.moneda_1*1)
        print("Hay ",self.moneda_050,' de 50 Cntvs.-',' = ',self.moneda_050*0.5)
        print("Hay ",self.moneda_020,' de 20 Cntvs.-',' = ',self.moneda_020*0.2)        
        print("Hay ",self.moneda_010,' de 10 Cntvs.-',' = ',self.moneda_010*0.1)        


caja1=Caja(10,2,3,4,5,2,3,5,7,20,33)
caja2=Caja(30,20,32,34,55,72,23,15,47,320,2)
caja3=Caja(2,1,1,1,1,1,1,1,1,1,1)
print('_____________________')
print('Monto total Caja1= ',caja1.calcular_caja())
print(caja1.mostrar_billetes())
print(caja1.mostrar_monedas())

print('=====================')
print('Monto total Caja2= ',caja2.calcular_caja())
print(caja2.mostrar_billetes())
print(caja2.mostrar_monedas())

print('=====================')
print('Monto total Caja3= ',caja3.calcular_caja())
print(caja3.mostrar_billetes_detalle())
print(caja3.mostrar_monedas_detalle())

if(caja1.calcular_caja()>caja2.calcular_caja()):
    print("Caja 1 es mayor")
else:
    print("Caja 2 es igual o menor")