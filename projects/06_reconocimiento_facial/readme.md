# Proyecto: Reconocimiento facial

Esta aplicación utiliza OpenCV para el reconocimiento facial en tiempo real a través de tu cámara web. Es una herramienta útil para aprender visión por computadora e inteligencia artificial.


![Employee data](image/imagen.gif)
## Descripción del Proyecto

El reconocimiento facial es una manera de identificar o confirmar la identidad de una persona mediante su rostro. Los sistemas de reconocimiento facial se pueden utilizar para identificar a las personas en fotos, videos o en tiempo real. El reconocimiento facial es una categoría de seguridad biométrica.

## Configuración y Ejecución del Proyecto

descargar la ultima version de python 3.11.04
instalar las librerias necesarias (opencv)
    ``` 
    pip install opencv-python 
    ```
- Ejecuta el archivo reconocimientoF.py para iniciar el programa
## Librerías Utilizadas

Este proyecto utiliza las siguientes librerías Python: opencv

La librería OpenCV proporciona un marco de trabajo de alto nivel para el desarrollo de aplicaciones de visión por computador en tiempo real: estructuras de datos, procesamiento y análisis de imágenes, análisis estructural, etc.

## Libro: PROGRAMA Y LIBERA TU POTENCIAL
El libro titulado 'Programa y Libera tu Potencial' ha sido nuestra guía fundamental, proporcionándonos un sólido fundamento en la comprensión de los elementos esenciales de los algoritmos y la programación en Python. Su enfoque en la liberación del potencial creativo a través de la codificación ha sido un pilar esencial en la concepción y desarrollo de este proyecto.
## Libro: PROGRAMA Y LIBERA TU POTENCIAL
El libro titulado 'Programa y Libera tu Potencial' ha sido nuestra guía fundamental, proporcionándonos un sólido fundamento en la comprensión de los elementos esenciales de los algoritmos y la programación en Python. Su enfoque en la liberación del potencial creativo a través de la codificación ha sido un pilar esencial en la concepción y desarrollo de este proyecto.

![Employee data](image/book.jpg)

Sobre los autores:
(http://programatupotencial.com)
## Agradecimientos

Queremos agradecer a nuestro docente, al decano de la facultad de ingeniería y a la Universidad Privada Domingo Savio - Sede (Santa Cruz) por su apoyo y orientación a lo largo de este proyecto.
## Cómo Contribuir
Como proyecto universitario, las contribuciones están limitadas a los miembros del equipo. Sin embargo, si encuentras algún error o tienes alguna sugerencia para mejorar el código o los análisis, no dudes en contactarte con el docente o el equipo de desarrollo.

Para cualquier pregunta o comentario, por favor contacta al correo electrónico (sc.jaime.zambrana.c@upds.net.bo).

## Equipo de desarrollo
[UNIVERSIDAD PRIVADA DOMINGO SAVIO - SANTA CRUZ](https://www.facebook.com/UPDS.bo)

Decano de la Facultad:
- [Msc. Wilmer Campos Saavedra](https://www.facebook.com/wilmercampos1)

Docente:
- [PhD. Jaime Zambrana Chacón](https://facebook.com/zambranachaconjaime)

Equipo de desarrollo:
- SEBASTIAN ROCA IBAÑEZ
- ERICK ARTEAGA ARAMAYO
- JULIO CESAR ARGANDOÑA MONTERO
- RODRIGO MEJIA HURTADO